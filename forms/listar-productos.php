<?php
	include_once("../lib/funciones.php");
	$conn = fn_conectar();
	
	$sentencia = 
	"select producto_id,
			producto.nombre as nombre_producto,
			producto.descripcion,
			tipo.nombre as nombre_tipo,
			marca.nombre as nombre_marca			
	from	producto, tipo, marca
	where	producto.tipo_id = tipo.tipo_id
	and		producto.marca_id = marca.marca_id
	order by 1";
	
	$rs = fn_ejecutar_sql($sentencia,$conn);
	if(!$rs){
		echo "No se existen productos";
		return;
	}
	
	$salida = "<table id='tabla'>
		<tr>
			<th>ID</th>
			<th>Nombre</th>
			<th>Descripción</th>
			<th>Tipo</th>
			<th>Marca</th>
		</tr>";
		
	foreach($rs as $fila){
		$salida .= "<tr>
			<td>" . $fila["producto_id"] . "</td>
			<td>" . $fila["nombre_producto"] . "</td>
			<td>" . $fila["descripcion"] . "</td>
			<td>" . $fila["nombre_tipo"] . "</td>
			<td>" . $fila["nombre_marca"] . "</td>
			<td><a href='#'>Borrar</a></td>
			<td><a href='#'>Editar</a></td>
		</tr>";
	}
	$salida .= "</table>
	<a href='insertar-producto.php' class='centrado'>Insertar</a>";
	
	echo $salida;
	
	fn_desconectar($conn);
?>