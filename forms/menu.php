<!DOCTYPE html>
<html>
	<head>
		<script type="text/javascript" src="../js/jquery.min.js"></script>
		<link rel="stylesheet" type="text/css" href="../css/estilo.css">
	</head>
	<body>
		<h1>Menu</h1>
		<div id="ajax-rs"></div>
	</body>
	<script>
		function listarProductos(){
			$.ajax({
				url:  "listar-productos.php",
				type: "post",
				success: function(response){
					$("#ajax-rs").html(response);
				}
			});
		}
		
		listarProductos();

	</script>
</html>